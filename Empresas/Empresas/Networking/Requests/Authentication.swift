//
//  Authentication.swift
//  Empresas
//
//  Created by Jefferson Lalor on 13/02/20.
//  Copyright © 2020 Lalor. All rights reserved.
//

import Foundation
import UIKit

class Authentication: APIRequest {
    override init(method: API.HTTPMethod, path: String, parameters: [String : Any]?, urlParameters: [String : Any]?, cacheOption: API.CacheOption, completion: ResponseBlock<Any>?) {
        super.init(method: method, path: path, parameters: parameters, urlParameters: urlParameters, cacheOption: cacheOption, completion: completion)
        
        self.baseURL = URL(string: "\(APIRequest.Constants.baseURL)/\(APIRequest.Constants.apiPath)/")!
    }
    
    @discardableResult
    static func loginWith(username: String, password: String, callback: ResponseBlock<Any>?) -> Authentication {
        
        let request = Authentication(method: .post, path: "users/auth/sign_in", parameters: ["email": username, "password": password], urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            
            NotificationCenter.default.post(name: .finishRequest, object: nil)

            if error != nil {
                NotificationCenter.default.post(name: .loginError, object: nil)
            } else {
                NotificationCenter.default.post(name: .loginSuccess, object: nil)
                callback?(nil, nil, cache)
            }
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        
        return request
    }
}

