//
//  Search.swift
//  Empresas
//
//  Created by Jefferson Lalor on 13/02/20.
//  Copyright © 2020 Lalor. All rights reserved.
//

import Foundation
import UIKit

class Search: APIRequest {
    override init(method: API.HTTPMethod, path: String, parameters: [String : Any]?, urlParameters: [String : Any]?, cacheOption: API.CacheOption, completion: ResponseBlock<Any>?) {
        super.init(method: method, path: path, parameters: parameters, urlParameters: urlParameters, cacheOption: cacheOption, completion: completion)
        
        self.baseURL = URL(string: "\(APIRequest.Constants.baseURL)/\(APIRequest.Constants.apiPath)/")!
    }
    
    @discardableResult
    static func enterpriseBy(name: String, callback: ResponseBlock<[Enterprise]>?) -> Search {
        let request = Search.init(method: .get, path: "enterprises", parameters: nil, urlParameters: ["name": name], cacheOption: .networkOnly) { (response, error, isCache) in
            
            let jData = try! JSONSerialization.data(withJSONObject: response!, options: .prettyPrinted)
            Search.decodeData(data: jData, key: "enterprises")
        }
        
        request.shouldSaveInCache = false
        request.makeRequest()
        
        return request
    }
    
    static func decodeData(data: Data, key: String) {
        let jsonDecoder = JSONDecoder()
        
        do {
            enterprises = try jsonDecoder.decode([Enterprise].self, from: data, keyedBy: key)
            
            if enterprises.isEmpty {
                NotificationCenter.default.post(name: .emptyResultsForSearchByEnterprises, object: nil)
            } else {
                NotificationCenter.default.post(name: .newResultsForSearchByEnterprises, object: nil)
            }
        } catch {print("Erro no decoder")}
    }

    
}
