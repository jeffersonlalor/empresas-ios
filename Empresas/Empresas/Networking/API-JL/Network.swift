//
//  Network.swift
//  Empresas
//
//  Created by Jefferson Lalor on 21/01/20.
//  Copyright © 2020 Lalor. All rights reserved.
//

import Foundation


public class Network {
    
    public static let shared = Network.init()
    public var requestReponse: RequestResponse?
    typealias api = InformationAPI
    
    private var user: User? {
        didSet {
            NotificationCenter.default.post(name: .loginSuccess, object: nil)
        }
    }

    private func dataTask(request: URLRequest, completion: @escaping () -> () ) {
        let task = URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            
            self.requestReponse = RequestResponse.init()
            self.requestReponse?.data = data
            self.requestReponse?.urlResponse = urlResponse
            self.requestReponse?.error = error
            
            completion()
            self.requestReponse = nil
            NotificationCenter.default.post(name: .finishRequest, object: nil)
        }
        
        task.resume()
    }
    
    public func login(email: String, password: String) {
        guard let url = URL.init(string: "\(api.dev_host.description)/api/\(api.api_version.description)/\(api.path_signIn.description)") else {return}
        
        var request = URLRequest.init(url: url)
        request.httpMethod = Method.post.description
        
        let parameters: [String: Any] = [
            "email" : "\(email)",
            "password" : "\(password)"
        ]
        
        request.httpBody = parameters.percentEncoded()
        
        self.dataTask(request: request) {
            self.verifyStatusError {
                self.setHeaders()
            }
        }
    }
    
    private func verifyStatusError(completion: @escaping () -> ()) {
        guard let response = self.requestReponse else {return}
        guard let urlResponse = response.urlResponse as? HTTPURLResponse, response.error == nil else {return}
        guard urlResponse.statusCode == 200 else {
            NotificationCenter.default.post(name: .loginError, object: nil)
            return
        }
        
        completion()
    }
    
    private func setHeaders() {
        guard let response = self.requestReponse else {return}
        guard let urlResponse = response.urlResponse as? HTTPURLResponse, response.error == nil else {return}
        
        guard let accessToken = urlResponse.value(forHTTPHeaderField: "access-token") else {return}
        guard let client = urlResponse.value(forHTTPHeaderField: "client") else {return}
        guard let uid = urlResponse.value(forHTTPHeaderField: "uid") else {return}
        
        self.user = User(access_token: accessToken, client: client, uid: uid)
    }
    
    
    public func searchEnterprise(name: String) {
        guard let url = URL.init(string: "\(api.dev_host.description)/api/\(api.api_version.description)/\(api.query_name.description)\(name)") else {return}
        guard let userPass = self.user else {return}

        var request = URLRequest.init(url: url)
        request.httpMethod = Method.get.description
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(userPass.access_token, forHTTPHeaderField: "access-token")
        request.addValue(userPass.client, forHTTPHeaderField: "client")
        request.addValue(userPass.uid, forHTTPHeaderField: "uid")
        
        self.dataTask(request: request) {
            guard let response = self.requestReponse else {return}
            guard let data =  response.data, response.error == nil else {return}
            
            self.decodeData(data: data, key: "enterprises")
        }
    }
    
    private func decodeData(data: Data, key: String) {
        let jsonDecoder = JSONDecoder()
        
        do {
            enterprises = try jsonDecoder.decode([Enterprise].self, from: data, keyedBy: key)
            
            if enterprises.isEmpty {
                NotificationCenter.default.post(name: .emptyResultsForSearchByEnterprises, object: nil)
            } else {
                NotificationCenter.default.post(name: .newResultsForSearchByEnterprises, object: nil)
            }
        } catch {print("Erro no decoder")}
    }
    
        
    public func downloadImage() {
        
    }
    
}
