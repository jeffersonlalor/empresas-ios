//
//  InformationAPI.swift
//  Empresas
//
//  Created by Jefferson Lalor on 22/01/20.
//  Copyright © 2020 Lalor. All rights reserved.
//

import Foundation

public enum InformationAPI: CustomStringConvertible {
    case dev_host
    case api_version
    case path_signIn
    case query_name
    
    public var description: String {
        switch self {
        case .dev_host: return "https://empresas.ioasys.com.br"
        case .api_version: return "v1"
        case .path_signIn: return "users/auth/sign_in"
        case .query_name: return "enterprises?name="
        }
    }
    
}
