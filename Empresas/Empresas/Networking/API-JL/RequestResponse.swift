//
//  RequestResponse.swift
//  Empresas
//
//  Created by Jefferson Lalor on 23/01/20.
//  Copyright © 2020 Lalor. All rights reserved.
//

import Foundation

public struct RequestResponse {
    public var data: Data?
    public var urlResponse: URLResponse?
    public var error: Error?
}
