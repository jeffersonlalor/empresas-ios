//
//  File.swift
//  Empresas
//
//  Created by Jefferson Lalor on 22/01/20.
//  Copyright © 2020 Lalor. All rights reserved.
//

import Foundation

public enum Method: CustomStringConvertible {
    case post
    case get
    
    public var description: String {
        switch self {
        case .post: return "POST"
        case .get: return "GET"
        }
    }
    

}
