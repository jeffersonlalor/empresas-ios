//
//  DecodableRoot.swift
//  Empresas
//
//  Created by Jefferson Lalor on 23/01/20.
//  Copyright © 2020 Lalor. All rights reserved.
//

import Foundation

struct DecodableRoot<T>: Decodable where T: Decodable {
    
    private struct CodingKeys: CodingKey {
        var stringValue: String
        var intValue: Int?
        
        init?(stringValue: String) {
            self.stringValue = stringValue
        }
        
        init?(intValue: Int) {
            self.intValue = intValue
            stringValue = "\(intValue)"
        }
        
        static func key(named name: String) -> CodingKeys? {
            return CodingKeys(stringValue: name)
        }
        
    }
    
    let value: T
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        guard
          let keyName = decoder.userInfo[.jsonDecoderRootKeyName] as? String,
          let key = CodingKeys.key(named: keyName) else {
            throw DecodingError.valueNotFound(T.self, DecodingError.Context.init(codingPath: [], debugDescription: "Value not found at root level"))
        }
 
        value = try container.decode(T.self, forKey: key)
    }
    
}

