//
//  ViewController.swift
//  Empresas
//
//  Created by Jefferson de Oliveira Lalor on 13/12/19.
//  Copyright © 2019 Lalor. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setStyle()
    }
    
    private func setStyle() {
        self.navigationItem.titleView = Components.shared.logoNav()
        self.navigationController?.navigationBar.barStyle = .black
    }

}

