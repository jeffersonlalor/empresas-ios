//
//  SearchViewController.swift
//  Empresas
//
//  Created by Jefferson de Oliveira Lalor on 14/12/19.
//  Copyright © 2019 Lalor. All rights reserved.
//

import Foundation
import UIKit


class SearchViewController: UIViewController, UISearchBarDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblInformationSearch: UILabel!
    
    let identifier: String = "company-idcell"
    var searchBar = Components.shared.searchBar()
//    let network = Network.shared
    var timer: Timer?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchBar.delegate = self
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.setStyle()
        self.setNotifications()
    }
    
    private func setStyle() {
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationItem.hidesBackButton = true

        self.showSearchBar()
        self.hiddenLabel()
    }
    
    private func setNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableView), name: .newResultsForSearchByEnterprises, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showLabel), name: .emptyResultsForSearchByEnterprises, object: nil)
    }

    func showSearchBar() {
        self.searchBar.frame = CGRect.init(x: 0, y: 0, width: 200, height: 600)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: self.searchBar)
//        self.navigationItem.titleView = self.searchBar
        self.searchBar.searchTextField.becomeFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        enterprises = []
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        enterprises = []
        guard let text = searchBar.text else {return}
        guard text != "" else {
            self.reloadTableView()
            self.hiddenLabel()
            return
        }
        
        timer?.invalidate()

        if text.count < 5 {
            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(request), userInfo: nil, repeats: false)
        } else {
            self.request()
        }
    }
    
    @objc
    private func request() {
        guard let text = searchBar.text else {return}
        Search.enterpriseBy(name: text, callback: nil)
//        network.searchEnterprise(name: text)
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier != nil else {return}
        guard let companyViewController = segue.destination as? CompanyViewController else {return}
        
        guard let index = self.tableView.indexPathForSelectedRow else {return}
        
        let backItem = UIBarButtonItem.init()
        backItem.title = nil
        navigationItem.backBarButtonItem = backItem
        
        companyViewController.enterprise = enterprises[index.row]
    }
    
    @objc
    private func reloadTableView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    @objc
    private func showLabel() {
        DispatchQueue.main.async {
            self.lblInformationSearch.isHidden = false
        }
    }
    
    private func hiddenLabel() {
        self.lblInformationSearch.isHidden = true
    }

}
