//
//  SearchViewController+TableView.swift
//  Empresas
//
//  Created by Jefferson de Oliveira Lalor on 14/12/19.
//  Copyright © 2019 Lalor. All rights reserved.
//

import Foundation
import UIKit

extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = enterprises.count
        
        if count == 0 {
            DispatchQueue.main.async {
                self.tableView.isHidden = true
            }
        } else {
            DispatchQueue.main.async {
                self.tableView.isHidden = false
            }
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.identifier, for: indexPath) as? ResultTableViewCell else {return UITableViewCell.init()}
        
        cell.fill(enterprise: enterprises[indexPath.row])
        
        return cell
    }
    
    
}
