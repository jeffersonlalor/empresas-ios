//
//  CompanyViewController.swift
//  Empresas
//
//  Created by Jefferson de Oliveira Lalor on 14/12/19.
//  Copyright © 2019 Lalor. All rights reserved.
//

import Foundation
import UIKit

class CompanyViewController: UIViewController {
    
    @IBOutlet weak var imageViewCompany: UIImageView!
    @IBOutlet weak var textViewDetails: UITextView!
    
    public var enterprise: Enterprise!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fill()
    }
    
    private func fill() {
        self.navigationItem.title = self.enterprise.enterprise_name
        self.textViewDetails.backgroundColor = .white
        
        #warning("download image")
//        self.imageViewCompany.image = self.enterprise.image
        self.textViewDetails.text = self.enterprise.description
    }
}
