//
//  LoginViewController.swift
//  Empresas
//
//  Created by Jefferson de Oliveira Lalor on 14/12/19.
//  Copyright © 2019 Lalor. All rights reserved.
//

import Foundation
import UIKit
import Lottie


class LoginViewController: UIViewController {
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var lblErroInformation: UILabel!
    @IBOutlet weak var alphaView: UIView!
    
    
    let animationView = AnimationView.init(name: "load")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNotifications()
        self.setStyle()
        self.setTargets()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.setStyle()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        print("Saindo da LoginScreen")
    }
    
    @IBAction func buttonLoginPressed(_ sender: Any) {
        guard let email = self.textFieldEmail.text else {return}
        guard self.isValidEmail(email) else {
            self.messageError()
            return
        }
        guard let password = self.textFieldPassword.text else {
            self.messageError()
            return
        }
        
        self.btnLogin.loginButtonDisabled()
        self.setLoadAnimation()
        
        Authentication.loginWith(username: email, password: password, callback: nil)
    }
    
    @objc
    private func login() {
        DispatchQueue.main.async {
            let homeViewController: UIViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "navigationHomeID") as UIViewController
            self.present(homeViewController, animated: true, completion: nil)
        }
    }
    
    private func setStyle() {
        self.navigationController?.navigationBar.barStyle = .default
        self.btnLogin.layer.cornerRadius = 8.0
        self.btnLogin.loginButtonDisabled()
        self.textFieldEmail.setIcon(UIImage.init(named: "icEmail"))
        self.textFieldEmail.setBottomBorder()
        self.textFieldPassword.setIcon(UIImage.init(named: "icCadeado"))
        self.textFieldPassword.setBottomBorder()
        self.lblErroInformation.isHidden = true
        self.alphaView.isHidden = true
    }
    
    private func setNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(login), name: .loginSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(messageError), name: .loginError, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeLoadAnimation), name: .finishRequest, object: nil)
    }
    
    private func setTargets() {
        self.textFieldEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.textFieldPassword.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    @objc
    private func messageError() {
        self.textFieldEmail.highlightedActivate()
        self.textFieldPassword.highlightedActivate()
        self.hiddenErrorMessage(false)
    }
    
    @objc
    private func textFieldDidChange(_ textField: UITextField) {
        self.hiddenErrorMessage(true)
        self.textFieldEmail.highlightedDesactivate()
        self.textFieldPassword.highlightedDesactivate()
        
        if self.isValidEmail(self.textFieldEmail.text ?? "") && !(self.textFieldPassword.text?.isEmpty ?? true) {
            self.btnLogin.loginButtonEnabled()
        } else {
            self.btnLogin.loginButtonDisabled()
        }
    }
    
    private func hiddenErrorMessage(_ isHidden: Bool) {
        DispatchQueue.main.async {
            self.lblErroInformation.isHidden = isHidden
        }
    }
    
    private func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    private func setLoadAnimation() {
        self.alphaView.isHidden = false
        
        animationView.loopMode = .loop
        animationView.play()
        self.view.addSubview(animationView)

        animationView.translatesAutoresizingMaskIntoConstraints = false
        let centerX = NSLayoutConstraint.init(item: animationView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let centerY = NSLayoutConstraint.init(item: animationView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint.init(item: animationView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 300)
        let heightConstraint = NSLayoutConstraint.init(item: animationView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 300)
        
        let constraints: [NSLayoutConstraint] = [centerX, centerY, widthConstraint, heightConstraint]
        
        self.view.addConstraints(constraints)
    }
    
    @objc
    private func removeLoadAnimation() {
        DispatchQueue.main.async {
            self.alphaView.isHidden = true
            self.animationView.removeFromSuperview()
        }
    }
    
}
