//
//  EnterpriseType.swift
//  Empresas
//
//  Created by Jefferson Lalor on 22/01/20.
//  Copyright © 2020 Lalor. All rights reserved.
//

import Foundation

public struct EnterpriseType: Codable, Mappable {
    public var id: Int
    public var enterprise_type_name: String
    
    public init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.enterprise_type_name = mapper.keyPath("enterprise_type_name")
    }
}
