//
//  User.swift
//  Empresas
//
//  Created by Jefferson Lalor on 22/01/20.
//  Copyright © 2020 Lalor. All rights reserved.
//

import Foundation

public struct User {
    public var access_token: String
    public var client: String
    public var uid: String
}
