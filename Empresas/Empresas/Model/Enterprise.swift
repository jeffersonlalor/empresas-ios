//
//  Company.swift
//  Empresas
//
//  Created by Jefferson de Oliveira Lalor on 14/12/19.
//  Copyright © 2019 Lalor. All rights reserved.
//

import Foundation

public struct Enterprise: Codable {
    public var id: Int
    public var email_enterprise: String?
    public var facebook: String?
    public var twitter: String?
    public var linkedin: String?
    public var phone: String?
    public var own_enterprise: Bool
    public var enterprise_name: String?
    public var photo: String?
    public var description: String?
    public var city: String?
    public var country: String?
    public var value: Int?
    public var share_price: Double?
    public var enterprise_type: EnterpriseType
    
//    public init(mapper: Mapper) {
//        self.id = mapper.keyPath("enterprises.id")
//        self.email_enterprise = mapper.keyPath("enterprises.email_enterprise")
//        self.facebook = mapper.keyPath("enterprises.facebook")
//        self.twitter = mapper.keyPath("enterprises.twitter")
//        self.linkedin = mapper.keyPath("enterprises.linkedin")
//        self.phone = mapper.keyPath("enterprises.phone")
//        self.own_enterprise = mapper.keyPath("enterprises.own_enterprise")
//        self.enterprise_name = mapper.keyPath("enterprises.enterprise_name")
//        self.photo = mapper.keyPath("enterprises.photo")
//        self.description = mapper.keyPath("enterprises.description")
//        self.city = mapper.keyPath("enterprises.city")
//        self.country = mapper.keyPath("enterprises.country")
//        self.value = mapper.keyPath("enterprises.value")
//        self.share_price = mapper.keyPath("enterprises.share_price")
//        self.enterprise_type = mapper.keyPath("enterprise.enterprise_type")
//    }
}

public var enterprises: [Enterprise] = [] {
    didSet {
        
    }
}
