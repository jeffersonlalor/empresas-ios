//
//  JSONDecoder+Extension.swift
//  Empresas
//
//  Created by Jefferson Lalor on 23/01/20.
//  Copyright © 2020 Lalor. All rights reserved.
//

import Foundation

extension JSONDecoder {
    func decode<T: Decodable>(_ type: T.Type, from data: Data, keyedBy key: String?) throws -> T {
        if let key = key {
            // Pass the top level key to the decoder
            userInfo[.jsonDecoderRootKeyName] = key
            let root = try decode(DecodableRoot<T>.self, from: data)
            return root.value
        } else {
            return try decode(type, from: data)
        }
    }
    
}

extension CodingUserInfoKey {
    static let jsonDecoderRootKeyName = CodingUserInfoKey(rawValue: "rootKeyName")!
}
