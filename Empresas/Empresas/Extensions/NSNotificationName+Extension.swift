//
//  NSNotificationName+Extension.swift
//  Empresas
//
//  Created by Jefferson Lalor on 22/01/20.
//  Copyright © 2020 Lalor. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    public static let loginSuccess = NSNotification.Name.init("com.lalor.Empresas-loginSuccess")
    public static let loginError = NSNotification.Name.init("com.lalor.Empresas-loginError")
    public static let newResultsForSearchByEnterprises = NSNotification.Name.init("com.lalor.Empresas-newResultsForSearchByEnterprises")
    public static let emptyResultsForSearchByEnterprises = NSNotification.Name.init("com.lalor.Empresas-emptyResultsForSearchByEnterprises")
    public static let finishRequest = NSNotification.Name.init("com.lalor.Empresas-finishRequest")
}
