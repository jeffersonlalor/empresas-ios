//
//  ResultTableViewCell.swift
//  Empresas
//
//  Created by Jefferson de Oliveira Lalor on 14/12/19.
//  Copyright © 2019 Lalor. All rights reserved.
//

import Foundation
import UIKit


class ResultTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageList: UIImageView!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblTypeCompany: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    
    public func fill(enterprise: Enterprise) {
        #warning("Download Image")
//        self.imageList.image = company.image
        self.lblCompanyName.text = enterprise.enterprise_name
        self.lblTypeCompany.text = enterprise.enterprise_type.enterprise_type_name
        self.lblCountry.text = enterprise.country
    }
    
}
