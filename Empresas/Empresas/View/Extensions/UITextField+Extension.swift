//
//  UITextField+Extension.swift
//  Empresas
//
//  Created by Jefferson de Oliveira Lalor on 14/12/19.
//  Copyright © 2019 Lalor. All rights reserved.
//

import Foundation
import UIKit


extension UITextField {
    public func setIcon(_ image: UIImage?) {
        let iconView = UIImageView.init(image: image)
        let viewContainer = UIView.init()
        
        self.tintColor = UIColor.init(red: 238/255.0, green: 76/255.0, blue: 119/255.0, alpha: 1.0)
        
        viewContainer.frame.size = CGSize.init(width: iconView.frame.size.width + 10.0, height: iconView.frame.size.height)
        viewContainer.addSubview(iconView)
        
        self.leftView = viewContainer
        self.leftViewMode = .always
    }
    
    public func setBottomBorder() {
        let border = CALayer()
        let width = CGFloat(0.9)
        
        border.borderColor = UIColor.init(red: 56.0/255.0, green: 55.0/255.0, blue: 67.0/255.0, alpha: 1.0).cgColor
        border.frame = CGRect.init(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        border.name = "borderTextField"
        
        self.borderStyle = .none
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    public func highlightedActivate() {
        DispatchQueue.main.async {
            guard let sublayers = self.layer.sublayers else {return}
            
            sublayers.forEach { (layer) in
                if layer.name == "borderTextField" {
                    layer.borderColor = UIColor.init(red: 255.0/255.0, green: 15.0/255.0, blue: 68.0/255.0, alpha: 1.0).cgColor
                }
            }
        }
    }
    
    public func highlightedDesactivate() {
        DispatchQueue.main.async {
            guard let sublayers = self.layer.sublayers else {return}
            
            sublayers.forEach { (layer) in
                if layer.name == "borderTextField" {
                    layer.borderColor = UIColor.init(red: 56.0/255.0, green: 55.0/255.0, blue: 67.0/255.0, alpha: 1.0).cgColor
                }
            }
        }
    }
    
}
