//
//  UIButton+Extension.swift
//  Empresas
//
//  Created by Jefferson Lalor on 24/01/20.
//  Copyright © 2020 Lalor. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    public func loginButtonEnabled() {
        self.isEnabled = true
        self.backgroundColor = UIColor.init(red: 87.0/255.0, green: 187.0/255.0, blue: 188.0/255, alpha: 1.0)
    }

    public func loginButtonDisabled() {
        self.isEnabled = false
        self.backgroundColor = UIColor.init(red: 116.0/255.0, green: 131.0/255.0, blue: 131.0/255.0, alpha: 1.0)
    }
}
