//
//  Components.swift
//  Empresas
//
//  Created by Jefferson de Oliveira Lalor on 13/12/19.
//  Copyright © 2019 Lalor. All rights reserved.
//

import Foundation
import UIKit


public struct Components {
    public static let shared: Components = Components.init()
    
    public func logoNav() -> UIImageView {
        return UIImageView.init(image: UIImage.init(named: "logoNav"))
    }
    
    public func searchBar() -> UISearchBar {
        let searchBar = UISearchBar.init(frame: CGRect.init(x: 0, y: 0, width: 200, height: 1000))
        
        searchBar.alpha = 0
        searchBar.showsCancelButton = true
        searchBar.placeholder = "Pesquisar"
        searchBar.tintColor = .white
        
        searchBar.searchTextField.backgroundColor = .white
        searchBar.searchTextField.textColor = .black
        
        UIView.animate(withDuration: 0.5) {
            searchBar.alpha = 1
        }

        return searchBar
    }

}
